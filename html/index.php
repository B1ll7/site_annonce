<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid m-0 p-0">
            <div class="header d-inline-flex m-3">
                <h1 class="">MonSiteD'annonce</h1>
                <div class="d-inline-flex">
                    <div><a href="">Page d'Accueil</a></div>
                    <div><a href="">Consulter une annonce</a></div>
                    <div><a href="">Contacts</a></div>
                </div>        
                <div class="">Admin</div>
            </div>
            <div class="body">

                <div class="row mb-5">
                    <div class="col-md-3 order-md-1 mb-4 ml-5">
                        <h2>Selection de votre critère</h2>
                        <ul>
                            <li><a href="">Annonces par Rubriques</a></li>
                            <li><a href="">Annonces par utilisateur</a></li>
                            <li><a href="">Gerer mes annonces</a></li>
                            <li><a href="">Deposer une annonce</a></li>
                        </ul>
                    </div>
                    <!-- bloc des annonces -->
                    <div class="col-md-8 order-md-2">
                        <div class="row ml-5"> 
                            <p class="col"><a href="">Gerer les rubriques</a></p>
                            <p class="col"><a href=""> Gerer les annonces périmées</a></p>
                            <p class="col"><a href="">Reinitialiser les mots de passe utilisateur</a></p>
                        </div>
                        <!--Annonce numero 1 -->
                        <div class="row mt-3 mb-3 p-4 bg-secondary">
                            <div class="col-md-2 bg-light"><img src="#" alt="ceci est une image" /></div>
                            <div class="col">
                                <div>Ceci est un libéllé</div>
                                <div>Ceci est une rubrique</div>
                                <div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nemo ratione, similique perferendis facilis officia? Quam aut fugit consequatur, tempora error et, amet dignissimos excepturi libero, consequuntur at illum sint!</p>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat eaque deleniti repudiandae sed aliquid et delectus officiis id rem odit, dolorem exercitationem vel assumenda nam, error mollitia incidunt velit minima!</p>
                                    <div class="row text-center">
                                        <div class="col">ceci est une date</div>
                                        <div class="col">ceci est une autre date</div>
                                    </div>
                                </div>
                            </div>   
                        </div>  
                        <!--annonce numero 2 -->
                        <div class="row mt-3 mb-3 p-4 bg-secondary">
                            <div class="col-md-2 bg-light"><img src="#" alt="ceci est une image" /></div>
                            <div class="col">
                                <div>Ceci est un libéllé</div>
                                <div>Ceci est une rubrique</div>
                                <div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nemo ratione, similique perferendis facilis officia? Quam aut fugit consequatur, tempora error et, amet dignissimos excepturi libero, consequuntur at illum sint!</p>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat eaque deleniti repudiandae sed aliquid et delectus officiis id rem odit, dolorem exercitationem vel assumenda nam, error mollitia incidunt velit minima!</p>
                                    <div class="row text-center">
                                        <div class="col">ceci est une date</div>
                                        <div class="col">ceci est une autre date</div>
                                    </div>
                                </div>
                            </div>   
                        </div> 
                        <!--annonce numero 3 -->
                        <div class="row mt-3 mb-3 p-4 bg-secondary">
                            <div class="col-md-2 bg-light"><img src="#" alt="ceci est une image" /></div>
                            <div class="col">
                                <div>Ceci est un libéllé</div>
                                <div>Ceci est une rubrique</div>
                                <div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nemo ratione, similique perferendis facilis officia? Quam aut fugit consequatur, tempora error et, amet dignissimos excepturi libero, consequuntur at illum sint!</p>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat eaque deleniti repudiandae sed aliquid et delectus officiis id rem odit, dolorem exercitationem vel assumenda nam, error mollitia incidunt velit minima!</p>
                                    <div class="row text-center">
                                        <div class="col">ceci est une date</div>
                                        <div class="col">ceci est une autre date</div>
                                    </div>
                                </div>
                            </div>   
                        </div> 
                    </div>
                </div>
            </div>
            <div class="bg-dark w-100 p-2 mb-0 text-light text-center fixed-bottom">
                <p>Ceci edt un footer<p>
            </div>
        </div>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    </body>
</html>